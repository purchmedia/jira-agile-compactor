// ==UserScript==
// @name              Purch Jira Agile Compactor
// @version           0.4.9
// @description       CSS overrides for Jira Agile for a denser display
// @author            Sébastien Volle, Alexandre Marchais, Loïc Giraudel
// @downloadURL       https://bitbucket.org/purchmedia/jira-agile-compactor/raw/master/purch_jira_agile_dense.user.js
// @updateURL         https://bitbucket.org/purchmedia/jira-agile-compactor/raw/master/purch_jira_agile_dense.user.js
// @match             https://purch1.atlassian.net/secure/RapidBoard.jspa*
// @grant             none
// ==/UserScript==

(function() {
  "use strict";

  var cssOverrides = "\
   .ghx-issue .ghx-issue-content { min-height: 30px; padding: 5px 10px 5px 35px; overflow: hidden;}\
   .ghx-swimlane-header .ghx-heading { margin: 5px 0; }\
   .ghx-issue.ghx-has-avatar .ghx-issue-fields, .ghx-issue.ghx-has-corner .ghx-issue-fields { padding-right: 60px; }\
   .ghx-issue.ghx-has-avatar .ghx-avatar { padding-right: 25px; }\
   .ghx-issue .ghx-end { top: 10px; right: 5px; }\
   .ghx-column-headers .ghx-column:first-child { border-color: #4a6785;}\
   .ghx-column-headers .ghx-column {border-color: #f6c342;}\
   .ghx-column-headers .ghx-column:last-child {border-color: #14892c;}\
   .ghx-swimlane:not(.ghx-closed) .ghx-columns .ghx-column:first-child { background-color: rgba(74, 103, 133, 0.5); border-left: 4px solid rgb(74, 103, 133); border-right: 4px solid rgb(74, 103, 133);}\
   .ghx-swimlane:not(.ghx-closed) .ghx-columns .ghx-column { background-color: rgba(246, 195, 66, 0.5); border-left: 4px solid rgb(246, 195, 66); border-right: 4px solid rgb(246, 195, 66);}\
   .ghx-swimlane:not(.ghx-closed) .ghx-columns .ghx-column:last-child { background-color: rgba(20, 137, 44, 0.5); border-left: 4px solid rgb(20, 137, 44); border-right: 4px solid rgb(20, 137, 44); }\
   .ghx-issue-compact {background-color: #f3f3d1;}\
   .ghx-swimlane-header {background-color: #f3f3d1; border-bottom: 1px solid #dbdbb0 }\
   .ghx-swimlane-header:after {background: transparent; -webkit-box-shadow: none;box-shadow: none; -webkit-box-shadow: none;box-shadow: none; }\
   .ghx-done:not(.ghx-issue-subtask) {background-color: #daebcf;}\
   .ghx-extra-field[data-tooltip='Status: To Do'] {display: table-cell; background-color: #fff; border: 1px solid #e4e8ed; color: #4a6785; padding: 4px; font-size: 11px; font-weight: bold; height: 10px; padding-bottom: 8px; text-transform: uppercase;}\
   .ghx-extra-field[data-tooltip='Status: Done'] {display: table-cell; background-color: #fff; border: 1px solid #b2d8b9; color: #14892c; padding: 4px; font-size: 11px; font-weight: bold; height: 10px; padding-bottom: 8px; text-transform: uppercase;}\
   .ghx-extra-field[data-tooltip='Status: In Progress'] {display: table-cell; background-color: #fff; border: 1px solid #ffe28c; color: #594300; padding: 4px; font-size: 11px; font-weight: bold; height: 10px; padding-bottom: 8px; text-transform: uppercase;}\
   .ghx-issue-compact .ghx-end {background: transparent; -webkit-box-shadow: none;box-shadow: none; -webkit-box-shadow: none;box-shadow: none; }\
   .ghx-priority[title='None']{display:none}\
   .ghx-issue .ghx-extra-fields { display: none; }\
   .ghx-issue-fields .ghx-key { display: none; }\
   .ghx-issue-fields > div.ghx-summary { margin-top: 5px; }\
   .ghx-swimlane-header.ghx-done .ghx-description {text-decoration: line-through; }\
   .ghx-swimlane-header.ghx-done .ghx-summary {text-decoration: line-through; }\
   .ghx-swimlane-header.ghx-inprogress { background-color: rgb(246, 195, 66)}\
   .backlog-issue-inprogress { background-color: rgb(246, 195, 66)}\
  ";

  var s = document.createElement('style');
  s.textContent = cssOverrides;
  document.body.appendChild(s);
    
}());

    jQuery(function() {
        window.setTimeout(function() {
            jQuery('.ghx-swimlane-header + .ghx-columns').each(function() {
                console.log(jQuery(this).find('li:nth(2),li:nth(1)').find('.ghx-issue-subtask').length);
                if (jQuery(this).find('li:nth(2),li:nth(1)').find('.ghx-issue-subtask').length !== 0) {
                    jQuery(this).closest('.ghx-columns').parent().find('.ghx-swimlane-header:not(.ghx-done)').addClass('ghx-inprogress');
                }
            });
        }, 3000);
    });

		jQuery(".ghx-extra-field[data-tooltip='Status: In Progress']").closest(".ghx-issue-content").addClass('backlog-issue-inprogress');
